# ZipUnpacker

This project is a small webserver intended to be used as a webhook server in CD configurations.

It allows you to configure any number of HTTP POST endpoints linked to filesystem location. It will then accept Zip files post'ed to those locations and extract them.

It is possible to specify command to run before and after extraction.

## Example configuration

Configuration is done using a Groovy DSL.

The `ServerDSL.token` field is mandatory. Fill this with an access token you'd like to use to make POST requests.
You can have as many `PathDSL` sections as you like. `route` specifies the HTTP route

```groovy

ServerDSL.make {
    host "localhost"
    port 8081
    token "345vj8706y8m"
}

PathDSL.make {
    route "/docs"
    path "/home/dev/testing/documentation"
    before([
        'notify-send "Before extraction"',
        'systemctl --user stop testing.service'
    ])
    after([
        'notify-send "After extraction"',
        'systemctl --user stop testing.service'
    ])
    disable false
}
```

Start with

```bash
$ java -jar zipunpack-1.0.jar -c config.gdsl
```

Example request:

```bash
$ curl -X POST 'localhost:8081/docs?token=345vj8706y8m' --data-binary @Package.zip
```
