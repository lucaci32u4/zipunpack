package xyz.lucaci32u4.zipunpack

import groovy.transform.TypeChecked;

@TypeChecked
class ServerDSL {
    private static ServerDSL server = null

    private String host = "localhost"
    private int port = 8282
    private String token = null

    def static make(@DelegatesTo(ServerDSL) Closure closure) {
        ServerDSL dsl = new ServerDSL()
        closure.delegate = dsl
        closure()
        server = dsl
    }

    static ServerDSL getServer() {
        return server
    }

    void host(String host) {
        this.host = host
    }

    void port(int port) {
        this.port = port
    }

    void token(String token) {
        this.token = token
    }

    String getHost() {
        return host
    }

    int getPort() {
        return port
    }

    String getToken() {
        return token
    }


}
