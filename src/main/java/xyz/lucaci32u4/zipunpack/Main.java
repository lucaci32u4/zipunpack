package xyz.lucaci32u4.zipunpack;

import groovy.lang.GroovyShell;
import groovy.transform.TypeChecked;
import lombok.Getter;
import org.codehaus.groovy.control.CompilerConfiguration;
import org.codehaus.groovy.control.customizers.ASTTransformationCustomizer;
import org.codehaus.groovy.control.customizers.ImportCustomizer;
import org.codehaus.groovy.transform.ASTTransformation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import picocli.CommandLine;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.Callable;
import java.util.function.Supplier;

@CommandLine.Command(name = "ZipUnpacker", version = "ZipUnpacker 1.0", mixinStandardHelpOptions = true)
public class Main implements Callable<Integer> {
    public static final Logger logger = LoggerFactory.getLogger(Main.class);

    private final GroovyShell groovyShell = ((Supplier<GroovyShell>) () -> {
        ImportCustomizer importCustomizer = new ImportCustomizer();
        importCustomizer.addImport("PathDSL", "xyz.lucaci32u4.zipunpack.PathDSL");
        importCustomizer.addImport("ServerDSL", "xyz.lucaci32u4.zipunpack.ServerDSL");
        importCustomizer.addImport("TypeChecked", "groovy.transform.TypeChecked");
        CompilerConfiguration compilerConfiguration = new CompilerConfiguration();
        compilerConfiguration.addCompilationCustomizers(importCustomizer);
        compilerConfiguration.addCompilationCustomizers(new ASTTransformationCustomizer(TypeChecked.class));
        return new GroovyShell(compilerConfiguration);
    }).get();

    @CommandLine.Option(names = {"--config",  "-c"}, description = "Config file", required = true)
    @Getter
    private String configPath;

    @Override
    public Integer call() throws Exception {

        groovyShell.evaluate(new File(configPath));

        if (ServerDSL.getServer() == null) {
            logger.error("Missing ServerDSL section in config file");
            return -1;
        }

        if (ServerDSL.getServer().getToken() == null) {
            logger.error("Missing token field in ServerDSL section in config file");
            return -1;
        }

        if (PathDSL.getPaths().isEmpty()) {
            logger.warn("Config file does not have any PathDSL sections. Are you sure this is intended?");
        }

        Webserver webserver = new Webserver();
        webserver.start();
        webserver.join();
        return 0;
    }

    public static void main(String[] args) {
        int exitCode = new CommandLine(new Main()).execute(args);
        System.exit(exitCode);
    }
}
