package xyz.lucaci32u4.zipunpack

import groovy.transform.TypeChecked

@TypeChecked
class PathDSL {
    private static final ArrayList<PathDSL> paths = new ArrayList<>()

    private String route = null
    private String path = null
    private List<String> arrbefore = new ArrayList<>()
    private List<String> arrafter = new ArrayList<>()
    private boolean disabled = false

    def static make(@DelegatesTo(PathDSL) Closure closure) {
        PathDSL dsl = new PathDSL()
        closure.delegate = dsl
        closure()
        paths.add(dsl)
    }

    void route(String route) {
        this.route = route.startsWith('/') ? route : '/' + route;
    }

    void path(String path) {
        this.path = path
    }

    void disable(boolean disable) {
        this.disabled = disable
    }

    String getRoute() {
        return route
    }

    String getPath() {
        return path
    }

    void before(List<String> cmds) {
        this.arrbefore = cmds
    }

    void after(List<String> cmds) {
        this.arrafter = cmds
    }

    List<String> getBefore() {
        return arrbefore
    }

    List<String> getAfter() {
        return arrafter
    }

    boolean isThisDisabled() {
        return disabled
    }

    static ArrayList<PathDSL> getPaths() {
        return paths
    }
}
