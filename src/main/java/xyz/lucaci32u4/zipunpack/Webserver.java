package xyz.lucaci32u4.zipunpack;

import io.javalin.Javalin;
import io.javalin.http.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Webserver {
    public static final Logger logger = LoggerFactory.getLogger(Webserver.class);

    private final Javalin server;

    public Webserver() {
        server = Javalin.create();
        HashMap<String, PathDSL> mapp = new HashMap<>();
        for (PathDSL p : PathDSL.getPaths()) {
            if (mapp.containsKey(p.getRoute())) {
                logger.warn("Found duplicate path '{}', disabling duplicate", p.getRoute());
                p.disable(true);
            } else {
                if (!p.isThisDisabled()) {
                    mapp.put(p.getRoute(), p);
                }
            }
        }
        logger.info("Initializing with {} enabled paths out of {}", mapp.size(), PathDSL.getPaths().size());

        for (PathDSL pdsl : mapp.values()) {
            server.post(pdsl.getRoute(), ctx -> postZip(ctx, pdsl));
        }
    }

    public void start() {
        server.start(ServerDSL.getServer().getHost(), ServerDSL.getServer().getPort());
    }

    public void join() {
        try {
            server.jettyServer().server().getThreadPool().join();
        } catch (InterruptedException e) {
            logger.error("Error while joining webserver thread", e);
        }
    }

    private void postZip(Context ctx, PathDSL path) {
        String token = ctx.queryParam("token");
        if (!ServerDSL.getServer().getToken().equals(token)) {
            httpErrorResult(ctx, 400, "Invalid token");
            return;
        }

        // Run before commands
        runCommands(path.getBefore(), path.getRoute());

        // Ensure exists and is folder
        Path destination = Paths.get(path.getPath());
        if (Files.exists(destination)) {
            if (!Files.isDirectory(destination)) {
                logger.error("Destination path for route '{}' is not a folder", path.getRoute());
                httpErrorResult(ctx, 503, "Failed to unpack");
                return;
            }
        } else {
            try {
                Files.createDirectories(destination);
            } catch (IOException exception) {
                logger.error("Failed to create destination folder for route '{}'", path.getRoute());
                httpErrorResult(ctx, 503, "Failed to unpack");
                return;
            }
        }

        // Unpack
        try (ZipInputStream zis = new ZipInputStream(ctx.bodyAsInputStream())) {
            ZipEntry zipEntry = zis.getNextEntry();
            while (zipEntry != null) {
                if (zipEntry.getName().contains("..")) continue;
                Path newFile = destination.resolve(zipEntry.getName());
                if (zipEntry.isDirectory()) {
                    if (!Files.isDirectory(newFile) && !newFile.toFile().mkdirs()) {
                        throw new IOException("Failed to create directory " + newFile);
                    }
                } else {
                    // fix for Windows-created archives
                    Path parent = newFile.getParent();
                    if (!Files.isDirectory(parent) && !parent.toFile().mkdirs()) {
                        throw new IOException("Failed to create directory " + parent);
                    }

                    // write file content
                    try (FileOutputStream fos = new FileOutputStream(newFile.toFile())) {
                        zis.transferTo(fos);
                    }
                }
                zipEntry = zis.getNextEntry();
            }
            zis.closeEntry();
        } catch (IOException ioe) {
            logger.error("Failed to unpack for route '{}'", path.getRoute(), ioe);
        }

        // Run after commands
        runCommands(path.getAfter(), path.getRoute());

    }


    private void postZip(Context context) {
        /*Path unpackLocation = main.getUnpackLocation();
        String authhdr = context.header("UnpackAuth");
        String unpackSubloc = context.header("UnpackLocation");
        if (authhdr == null || !authhdr.equals(main.getAuthHeader()) || unpackSubloc == null || !locationRegex.matcher(unpackSubloc).matches()) {
            context.status(400); // go to hell
            return;
        }
        if (!Files.exists(unpackLocation) || !Files.isDirectory(unpackLocation)) {
            logger.error("Unpack location is unreacheable");
            context.status(503); // server error
            return;
        }
        Path destination = unpackLocation.resolve(unpackSubloc);
        if (Files.exists(destination)) {
            if (!Files.isDirectory(destination)) {
                context.status(409); // conflict
                return;
            }
        } else {
            try {
                Files.createDirectory(destination);
            } catch (IOException exception) {
                logger.error("Failed to create destination directory", exception);
                context.status(503); // server error
                return;
            }
        }

        ZipInputStream zis = new ZipInputStream(context.bodyAsInputStream());
        try {
            for (ZipEntry entry = zis.getNextEntry(); entry != null; entry = zis.getNextEntry()) {
                if (entry.isDirectory() || entry.getName().contains("/")) {
                    continue; // ignore directories
                }
                String filename = entry.getName();
                Path writePath = destination.resolve(filename);
                int len;
                OutputStream fos = Files.newOutputStream(writePath);
                byte[] buffer = new byte[4096];
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
                fos.close();
            }
            zis.closeEntry();
            zis.close();
            logger.info("Successfully unpacked minute");
        } catch (IOException ioe) {
            logger.error("Error unzipping file: {}", ioe.getMessage());
            context.status(503);
            return;
        }
*/
    }

    private static void httpErrorResult(Context ctx, int code, String message) {
        ctx.status(code);
        ctx.result(message);
        ctx.contentType("text/plain");
    }

    private static boolean runCommands(Collection<String> cmds, String path) {
        for (String cmd : cmds) {
            try {
                Process p = Runtime.getRuntime().exec(new String[] {"bash", "-c", cmd});
                if (!p.waitFor(20, TimeUnit.SECONDS)) {
                    logger.error("Timeout while running command '{}' for route '{}'", cmd, path);
                    p.destroy();
                    return false;
                }
                if (p.exitValue() != 0) {
                    logger.error("Command '{}' for route '{}' ended with exit code {}", cmd, path, p.exitValue());
                    return false;
                }
            } catch (IOException | InterruptedException exception) {
                logger.error("Caught unexpected exception while running command '{}' for route '{}'", cmd, path, exception);
                return false;
            }
        }
        return true;
    }

}
